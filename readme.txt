SOLUTION EXPLANATION

1. List the language you're using: Java

2. Document your assumptions:
	- If there's no digits in the input text, the result will be an empty list
	- Left zeros disappear (0125=125)
	- Only positive numbers will be taken, as the minus sign it's not consider as a digit
	- The numbers will be in ascending order
	
3. Explain your approach and how you intend to solve the problem:
	As the input text could be huge, i intend to use a regular expression to trim the non-digit characters of the input text,
	and use then a treeset to control the non repetition and order.
	
5. Explain the big-0 run time complexity of your solution. Justify your answer:
	The complexity of my solution is O(n) because of the forEach and the removeIf done in a LinkedList.
	If i have used the removeIf in an ArrayList it could have been of O(n2), 
	because for each element that is removed, all elements of higher indices are moved to fill the gap in the backing array