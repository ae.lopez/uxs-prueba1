package uxs.prueba1.function;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ExtractNumbersTest {

	@Test
	public void getNumbers_NoDuplicates_Ok() {
		final String text = "jhd-1.0sh658aksj+/9'kj05b%$!kaslWAD";
		List<Integer> resultListMock = Arrays.asList(0,1,5,9,658);
		assertEquals(resultListMock, ExtractNumbers.getNumbers(text));
	}
	
	@Test
	public void getNumbers_Duplicates_Ok() {
		final String text = "jhd-1.0sh658aksj+/9'kj05b%$!kaslWAD9&�0`^��ma69-09";
		List<Integer> resultListMock = Arrays.asList(0,1,5,9,69,658);
		assertEquals(resultListMock, ExtractNumbers.getNumbers(text));
	}
	
	@Test
	public void getNumbers_OnlyNumbers_ReturnsUniqueNumber() {
		final String text = "0584762889";
		List<Integer> resultListMock = Arrays.asList(584762889);
		assertEquals(resultListMock, ExtractNumbers.getNumbers(text));
	}
	
	@Test 
	public void getNumbers_NoNumbers_ReturnsEmptyList() {
		final String text = "jhd-.shaksj+/'kjb%$!kaslWAD&�`^��ma-";
		assertTrue(ExtractNumbers.getNumbers(text).isEmpty());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void getNumbers_NullEntry_ThrowsIllegalArgumentException() {
		ExtractNumbers.getNumbers(null);
	}
	
	@Test
	public void getNumbers_NullEntry_ExceptionMessageOk() {
		try {
			ExtractNumbers.getNumbers(null);
		} catch (IllegalArgumentException e) {
			assertEquals("The input text must not be null", e.getMessage());
		}
	}
}
