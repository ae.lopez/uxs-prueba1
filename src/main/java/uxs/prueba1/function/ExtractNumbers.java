package uxs.prueba1.function;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public final class ExtractNumbers {
	
	private ExtractNumbers() {
		throw new IllegalStateException("This is an utility class and cannot be instantiated");
	}

	/**
	 * Method to extract in a list the numbers contained in the input string and order them in ascending order 
	 * @param string
	 * @return list of the integers
	 * @throws IllegalArgumentException if the input text is a null
	 */
	public static List<Integer> getNumbers(String text) {	
		
		if (text == null) {
			throw new IllegalArgumentException("The input text must not be null");
		}
		
		final String REGEX = "\\D";
		List<String> numbers = new LinkedList<>(Arrays.asList(text.split(REGEX)));	
		
		Set<Integer> numbersOrderedSet = new TreeSet<>();
		
		numbers.removeIf(number -> number.isBlank() || number.isEmpty());
		numbers.forEach(number -> numbersOrderedSet.add(Integer.valueOf(number)));
		
		return new ArrayList<>(numbersOrderedSet);
	}
}
