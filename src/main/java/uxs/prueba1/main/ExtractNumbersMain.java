package uxs.prueba1.main;

import java.util.List;
import java.util.Scanner;

import uxs.prueba1.function.ExtractNumbers;

public class ExtractNumbersMain {
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter a string: ");
		String text = reader.nextLine();
		reader.close();
		List<Integer> numbers = ExtractNumbers.getNumbers(text);
		System.out.println(numbers);
	}
}
